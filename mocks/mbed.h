#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>

#define USBTX (0)
#define USBRX (0)

class Serial {
	public:
		Serial(unsigned char, unsigned char, unsigned char*, unsigned short) {}

		void printf(const char *format, ...) {
			va_list arg_p;
			va_start(arg_p, format);
			vfprintf(stdout, format, arg_p);
			va_end(arg_p);
		}
};
